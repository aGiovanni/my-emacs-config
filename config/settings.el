;;; settings.el ---- Configuración de Emacs:

;;; Commentary:
;; Configuraciones generales al entorno Emacs.

;;; Code:

;; Configuramos el tipo de entrada (UTF-8)
(set-language-environment "UTF-8")

;; Configuramos el método de escritura por defecto (Al presionar C-\)
(setq default-input-method "latin-1-prefix")

;; Simplificamos la toma de decisiones (?
(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'qrr 'query-replace-regexp)

;; Creamos el directorio en donde irán las copias de seguridad:
(defvar backup-directory "~/.emacs.d/backups/")
(make-directory backup-directory t)

;; Ahora guardamos las copias de seguridad en la carpeta que creamos:
(setq backup-by-copying t
      backup-directory-alist `((".*" . , backup-directory))
      auto-save-list-file-prefix (concat backup-directory ".backup-")
      auto-save-file-name-transforms `((".*" , backup-directory t)))

;; Auto-completado en el mini-búffer:
(setq ido-enable-flex-matching t
      ido-everywhere t)
(ido-mode t)

;; Cambiamos la manera en que Emacs maneja las tabulaciones:
(setq-default indent-tabs-mode nil
              tab-width 4)

;; Auto-completado en los paréntesis:
(electric-pair-mode t)

;; Desactivamos esta función para hacer que Dired abra archivos en
;; el búffer más grande en vez de crear otro nuevo:
(setq split-width-threshold nil)

;; Avisamos cuando abrimos archivos de más de 100 Mb
(setq large-file-warning-threshold 100000000)

;; Reducimos la frecuencia del recolector de basura
(defun ambrevar/reset-gc-cons-threshold ()
  "Temporarily reduce garbage collection during startup.  Inspect `gcs-done'."
  ;; (setq gc-cons-threshold (car (get 'gc-cons-threshold 'standard-value)))
  (setq gc-cons-threshold (* 16 1024 1024)))
(setq gc-cons-threshold (* 254 1024 1024))

(add-hook 'after-init-hook #'ambrevar/reset-gc-cons-threshold)

(setq default-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)
(defun ambrevar/reset-file-name-handler-alist ()
  "Temporarily disable the file name handler."
  (setq file-name-handler-alist default-file-name-handler-alist))
(add-hook 'after-init-hook #'ambrevar/reset-file-name-handler-alist)

(setq load-prefer-newer t)

;; Guardamos la sesión al momento de cerrar Emacs:
;; (desktop-save-mode t)

;;; settings.el ends here
