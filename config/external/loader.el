;;; init.el ---- External packages for Emacs

;;; Commentary:
;; Archivo de arranque de los demás archivos de configuración de
;; instalación y configuración de paquetes externos.

;;; Code:

;; Definimos los directorios de configuración:
(defvar load-core (concat user-external-dir "core/"))
(defvar load-add-on (concat user-external-dir "add-on/"))
(defvar load-lang (concat user-external-dir "lang/"))

;; Procedemos a cargar los paquetes externos:
;; =================== CORE - INTERNAL ====================
(load (concat load-core "use-package"))
(load (concat load-core "dired"))
(load (concat load-core "ibuffer"))
(load (concat load-core "org"))
(load (concat load-core "term"))
;; =================== CORE - EXTERNAL ====================
;; (load (concat load-core "flx")) ;; Optional
(load (concat load-core "undo-tree"))
(load (concat load-core "avy"))
(load (concat load-core "company"))
(load (concat load-core "flycheck"))
(load (concat load-core "projectile"))
(load (concat load-core "magit"))

;; ======================== ADD-ON ========================
(load (concat load-add-on "theme"))
(load (concat load-add-on "yasnippet"))
;; (load (concat load-add-on "treemacs"))

;; ========================= LANG =========================
(load (concat load-lang "web"))
(load (concat load-lang "python"))
(load (concat load-lang "lsp")) ;; Still a work in progress!

;;; loader.el ends here
