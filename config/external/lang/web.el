;;; web-setup.el ---- Web Mode:

;;; Commentary:
;; A collection of packages for web development.

;;; Code:

(use-package web-mode
  :ensure t
  :mode
  ("\\.blade.php\\'" . web-mode)
  :config
  (setq
   web-mode-markup-indent-offset 2
   web-mode-css-indent-offset 2
   web-mode-code-indent-offset 2
   web-mode-enable-auto-pairing nil
   web-mode-enable-auto-quoting nil
   web-mode-enable-auto-expanding t
   web-mode-enable-current-element-highlight t
   web-mode-enable-current-column-highlight t)
  (setq web-mode-engines-alist
	'(("php" . "\\.phtml\\'")
	  ("blade" . "\\.blade\\."))))

(use-package emmet-mode
  :ensure t
  :hook
  ((sgml-mode . emmet-mode)
   (css-mode . emmet-mode)
   (html-mode . emmet-mode)
   (vue-mode . emmet-mode)))

(use-package js
  :hook
  (js-mode . (lambda ()
               (flycheck-select-checker 'javascript-eslint)))
  :config
  (setq js-indent-level 2))

(use-package js2-mode
  :ensure t
  :mode
  ("\\.js\\'" . js2-mode)
  :config
  (setq js2-basic-offset 2))

(use-package vue-mode
  :ensure t
  :mode
  ("\\.vue\\'" . vue-mode)
  :hook
  ((mmm-mode . (lambda ()
                 (set-face-background 'mmm-default-submode-face nil)))))

;;; web.el ends here
