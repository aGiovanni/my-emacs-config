;;; python.el ---- A set of packages for improving Python

;;; Commentary:


;;; Code:
(use-package python
  :config
  (setq python-shell-interpreter "python3")
  (setq python-indent-offset 4))


;; ============================================================
;; ANACONDA-MODE [IDE features for Python]
;; ============================================================
;; https://github.com/proofit404/anaconda-mode

(use-package anaconda-mode
  :ensure t
  :hook
  ((python-mode . anaconda-mode)
   (python-mode . anaconda-eldoc-mode)))

;; ============================================================
;; COMPANY ANACONDA PACKAGE
;; ============================================================
;; https://github.com/proofit404/company-anaconda

(use-package company-anaconda
  :after (anaconda-mode company)
  :ensure t
  :hook
  (anaconda-mode . (lambda ()
                     (set (make-local-variable 'company-backends)
                          '((company-anaconda :with company-yasnippet)
                            (company-capf :with company-yasnippet)
                            (company-files :with company-yasnippet)
                            (company-dabbrev-code :with company-yasnippet)
                            (company-dabbrev :with company-yasnippet))))))

;;; python.el ends here

