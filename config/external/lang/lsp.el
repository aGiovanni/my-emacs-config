;;; lsp.el --- Language Server Protocol Support for Emacs

;;; Commentary:
;; ============================================================
;; LSP MODE PACKAGE
;; ============================================================
;; `lsp-mode' aims to provide IDE-like experience by providing optional
;; integration with the most popular Emacs packages like company,
;; flycheck and projectile.

;; https://github.com/emacs-lsp/lsp-mode

;;; Code:
(use-package lsp-mode
  :ensure t
  :commands lsp
  :hook
  ((vue-mode . lsp)
   (css-mode . lsp)
   (sgml-mode . lsp)
   (html-mode . lsp)))

(use-package lsp-ui
  :ensure t
  :after lsp-mode
  :bind (:map lsp-ui-mode-map
              ([remap xref-find-definitions] . 'lsp-ui-peek-find-references)
              ([remap xref-find-references] . 'lsp-ui-peek-find-references))
  :commands lsp-ui-mode
  :hook
  ((lsp-mode . lsp-ui-doc-mode)
   (lsp-ui-mode . flycheck-mode)))

(use-package company-lsp
  :ensure t
  :after lsp-mode
  :commands company-lsp
  :config
  (push '(company-lsp :with company-yasnippet) company-backends))

;; (use-package lsp-java
;;   :ensure t
;;   :after lsp-mode
;;   :config
;;   (add-hook 'java-mode-hook 'lsp))

;; (use-package dap-mode
;;   :ensure t
;;   :after lsp-mode
;;   :config
;;   (dap-mode t)
;;   (dap-ui-mode t))

;; (use-package dap-java :after (lsp-java))
;; (use-package lsp-java-treemacs :after (treemacs))

;;; lsp.el ends here
