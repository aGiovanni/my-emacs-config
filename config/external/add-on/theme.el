;;; theme.el ---- Theme from melpa

;;; Commentary:
;; ============================================================
;; KAOLIN THEME
;; ============================================================
;; Kaolin is a set of eye pleasing themes for GNU Emacs with support
;; for a large number of modes and external packages.
;; https://github.com/ogdenwebb/emacs-kaolin-themes

;;; Code:

;; (use-package kaolin-themes
;;   :ensure t
;;   :config
;;   (load-theme 'kaolin-aurora t)
;;   ;; User's custom configuration:
;;   (set-face-attribute 'mode-line-buffer-id nil
;;   		      :bold t)
;;   (set-face-attribute 'mode-line nil
;;   		      :box '(:line-width 2 :color "#1f272e")
;;   		      :bold t
;;   		      :background "#1f272e")
;;   (set-face-attribute 'mode-line-inactive nil
;; 		      :box '(:line-width 2 :color "#191f26")
;;   		      :bold nil
;;   		      :background "#191f26"))

(use-package gruvbox-theme
  :ensure t
  :config
  (load-theme 'gruvbox-light-hard t)
  ;; Remove the parentheses on mode-line position
  (setq mode-line-position
        '(;; %p print percent of buffer above top of window, o Top, Bot or All
          ;; (-3 "%p")
          ;; %I print the size of the buffer, with kmG etc
          ;; (size-indication-mode ("/" (-4 "%I")))
          ;; " "
          ;; %l print the current line number
          ;; %c print the current column
          (line-number-mode ("%l" (column-number-mode ":%c")))))
  ;; Custom mode-line
  (setq-default mode-line-format
                '("%e"
                  mode-line-front-space
                  mode-line-mule-info
                  mode-line-client
                  mode-line-modified
                  " "
                  mode-line-buffer-identification
                  " ["
                  mode-line-position
                  "] "
                  (:eval (concat "[" (projectile-project-name) "]"))
                  " "
                  mode-line-modes
                  mode-line-misc-info
                  mode-line-end-spaces
                  )))

;;; theme.el ends here
