;;; treemacs.el ---- A tree layout file explorer for Emacs

;;; Commentary:
;; ============================================================
;; TREEMACS PACKAGE
;; ============================================================
;; Treemacs is a file and project explorer similar to NeoTree or vim’s
;; NerdTree, but largely inspired by the Project Explorer in Eclipse.
;; https://github.com/Alexander-Miller/treemacs

;;; Code:
(use-package treemacs
  :ensure t)

;;; treemacs.el ends here
