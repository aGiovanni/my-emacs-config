;;; yasnippet.el ---- A template system for Emacs

;;; Commentary:
;; ============================================================
;; YASNIPPET PACKAGE
;; ============================================================
;; YASnippet is a template system for Emacs. It allows you to type an
;; abbreviation and automatically expand it into function templates

;; https://github.com/joaotavora/yasnippet

;;; Code:
(use-package yasnippet
  :ensure t
  :delight yas-minor-mode
  :config
  (yas-global-mode t))

(use-package yasnippet-snippets
  :after yasnippet
  :ensure t
  :config (yasnippet-snippets-initialize))

;;; yasnippet.el ends here
