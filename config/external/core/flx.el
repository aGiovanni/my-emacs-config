;;; flx.el ---- Fuzzy matching for Emacs

;;; Commentary
;; ============================================================
;; FLX PACKAGE
;; ============================================================
;; Fuzzy matching for Emacs ... a la Sublime Text.

;; https://github.com/lewang/flx

;;; Code:
(use-package flx-ido
  :ensure t
  :config
  (flx-ido-mode 1)
  ;; disable ido faces to see flx highlights.
  (setq ido-enable-flex-matching t)
  (setq ido-use-faces nil))

;;; flx.el ends here
