;;; term.el --- Summary

;;; Commentary:

;;; Code:

(use-package term
  :bind (:map term-raw-map
              ("M-o" . nil)))

;;; term.el ends here
