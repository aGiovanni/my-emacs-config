;;; ibuffer.el --- Activation and additional configuration to ibuffer

;;; Commentary:
;; ============================================================
;; IBUFFER
;; ============================================================
;; Mejora del búffer que nos permite visualizar y manipular
;; los búffers. También nos permite categorizar los búffers que
;; se encuentran abiertos.

;;; Code:

(use-package ibuffer
  :bind (:map ibuffer-mode-map
	      ("M-o" . nil)
	      ("C-o" . nil))
  :hook
  (ibuffer-mode . ibuffer/my-custom-ibuffer-config)
  :init
  ;; Categorizamos los buffers para mejorar la organización:
  (setq ibuffer-saved-filter-groups
	'(("user"
	   ("Org" (mode . org-mode))
	   ("Web Dev" (or (mode . html-mode)
			  (mode . css-mode)
			  (mode . js-mode)
			  (mode . js2-mode)
			  (mode . web-mode)
			  (mode . php-mode)))
	   ("Python" (or (mode . python-mode)
                     (mode . elpy-mode)
                     (name . "\*Python\*")))
       ("Java" (or (mode . java-mode)
                   (name . "$\\.java")))
	   ("SQL" (or (mode . sql-mode)))
	   ("MATLAB" (or (mode . matlab-mode)
			 (filename . "$\\.m")))
	   ("Shell Files" (or (mode . shell-script-mode)))
	   ("Dired" (mode . dired-mode))
	   ("Git" (or (name . "magit")
		      (filename . "README.md")))
	   ("Shell" (or (name . "\*Shell\*")
			(mode . eshell-mode)))
	   ("Emacs-Config" (or (filename . ".emacs.d")
			       (mode . emacs-lisp-mode)))
	   ("Lisp" (mode . lisp-mode))
	   ("Help/Misc" (name . "\*.*\*")))))
  ;; Ocultamos las categorías que están vacías:
  (setq ibuffer-show-empty-filter-groups nil)
  ;; Ahora las categorías las guardamos en un hook:
  ;; También hacemos que la lista se actualice automáticamente:
  (defun ibuffer/my-custom-ibuffer-config ()
    (ibuffer-auto-mode 1)
    (ibuffer-switch-to-saved-filter-groups "user"))
  :config
  ;; Use human readable Size column instead of original one
  (define-ibuffer-column size-h
    (:name "Size" :inline t)
    (cond
     ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
     ((> (buffer-size) 100000) (format "%7.0fk" (/ (buffer-size) 1000.0)))
     ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
     (t (format "%8d" (buffer-size)))))
  ;; Modify the default ibuffer-formats
  (setq ibuffer-formats
	'((mark modified read-only " "
		(name 18 18 :left :elide)
		" "
		(size-h 9 -1 :right)
		" "
		(mode 16 16 :left :elide)
		" "
		filename-and-process))))

;; Hacemos de ibuffer el listado de buffers por defecto:
(defalias 'list-buffers 'ibuffer)

;;; ibuffer.el ends here
