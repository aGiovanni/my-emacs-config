;;; magit.el ---- It's Magit! A Git porcelain inside Emacs

;;; Commentary:
;; ============================================================
;; MAGIT PACKAGE
;; ============================================================
;; Magit is an interface to the version control system Git,
;; implemented as an Emacs package. Magit aspires to be a complete Git
;; porcelain.

;; https://github.com/magit/magit

;;; Code:
(use-package magit
  :ensure t
  :bind
  ("C-x g" . 'magit-status)
  ("C-x M-g" . 'magit-dispatch-popup))

;;; magit.el ends here
