;;; org.el ---- Additional configuration to Dired

;;; Commentary:
;; ============================================================
;; ORG MODE
;; ============================================================

;;; Code:
(use-package org
  :bind
  ("\C-cl" . 'org-store-link)
  ("\C-ca" . 'org-agenda)
  :config
  (setq org-src-fontify-natively t
        org-src-tab-acts-natively t
        org-edit-src-content-indentation 0)
  (setq org-agenda-files (directory-files-recursively "~/Documents/org/" "\.org$")))

;;; org.el ends here
