;;; dired.el ---- Additional configuration to Dired

;;; Commentary:
;; ============================================================
;; DIRED
;; ============================================================
;; Algunas configuraciones por parte del usuario para hacer
;; más cómodo navegar y manipular archivos y directorios.

;;; Code:

(use-package dired
  :bind (:map dired-mode-map
	      ("RET" . 'dired-find-alternate-file)
	      ("f" . 'dired-find-alternate-file)
	      ("^" . (lambda ()
		       (interactive)
		       (find-alternate-file ".."))))
  :hook
  (dired-mode . dired/my-custom-config)
  :init
  (defun dired/my-custom-config ()
    (dired-hide-details-mode t))
  :config
  ;; Para copiar y eliminar directorios:
  (setq dired-recursive-copies (quote always)
	dired-recursive-deletes (quote top))
  ;; Para copiar/mover de un directorio a otro:
  (setq dired-dwim-target t)
  (setq dired-listing-switches "-aBhl --group-directories-first")
  ;; Abrimos directorios con 'a' en Dired para reutilizar el mismo búffer:
  (put 'dired-find-alternate-file 'disabled nil))

;;; dired.el ends here
