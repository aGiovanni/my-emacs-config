;;; undo-tree.el ---- More undo/redo functions

;;; Commentary:
;; ============================================================
;; UNDO-TREE PACKAGE:
;; ============================================================
;; https://elpa.gnu.org/packages/undo-tree.html

;;; Code:
(use-package undo-tree
  :ensure t
  :delight
  :config
  (global-undo-tree-mode))

;;; undo-tree.el ends here
