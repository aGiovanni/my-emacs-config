;;; projectile.el ---- Project Interaction Library for Emacs

;;; Commentary:
;; ============================================================
;; PROJECTILE PACKAGE
;; ============================================================
;; `projectile' is a project interaction library for Emacs. Its goal is
;; to provide a nice set of features operating on a project level
;; without introducing external dependencies (when feasible).

;; https://github.com/bbatsov/projectile

;;; Code:
(use-package projectile
  :ensure t
  ;; :delight '(:eval (concat " [" (projectile-project-name) "]"))
  :delight
  :bind (:map projectile-mode-map
	      ("C-c p" . 'projectile-command-map))
  :config
  (projectile-mode +1))

;;; projectile.el ends here
