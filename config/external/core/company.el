;;; company.el ---- Modular in-buffer completion framework for Emacs

;;; Commentary:
;; ============================================================
;; COMPANY MODE - PACKAGE
;; ============================================================
;; Company is a text completion framework for Emacs. The name stands
;; for "complete anything". It uses pluggable back-ends and front-ends
;; to retrieve and display completion candidates.

;; https://company-mode.github.io/

;;; Code:
(use-package company
  :ensure t
  :delight
  :hook ((after-init . global-company-mode))
  :bind (:map company-mode-map
	      ("M-/" . #'company-complete-common-or-cycle))
  :config
  (setq company-idle-delay nil
	company-show-numbers t
	company-minimum-prefix-length 1
	company-tooltip-limit 8
	company-dabbrev-downcase nil)
  ;; Agremamos soporte a yasnippet para todos los backends.
  ;; https://github.com/syl20bnr/spacemacs/pull/179
  (defvar company-mode/enable-yas t
    "Enable yasnippet for all backends.")
  (declare-function 'company-mode/backend-with-yas 'company)
  (defun company-mode/backend-with-yas (backend)
    (if (or (not company-mode/enable-yas)
	    (and (listp backend)
		 (member 'company-yasnippet backend)))
	backend
      (append (if (consp backend) backend (list backend))
	      '(:with company-yasnippet))))
  (setq company-backends
	(mapcar #'company-mode/backend-with-yas company-backends)))

;;; company.el ends here
