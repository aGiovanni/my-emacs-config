;;; use-package.el ---- Essential packages for Emacs

;;; Commentary:
;; Paquetes externos a Emacs que son esenciales para mejorar la productividad

;;; Code:

;; Añadimos repositorios externos para descargar y configurar los paquetes:
(require 'package)
(add-to-list 'package-archives
	     '("melpa-stable" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

;; Establecemos prioridad para 'melpa-stable'
(setq-default package-archive-priorities
	      '(("melpa-stable" . 10)
            ("melpa" . 5)))

;; Actualizamos los repositorios
(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

;; ============================================================
;; USE-PACKAGE
;; ============================================================

;; Instalamos *use-package* para instalar y mantener otros paquetes:
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; Reducimos el tiempo de carga de Emacs:
(eval-when-compile
  (require 'use-package))

;; ============================================================
;; [USE-PACKAGE] - DELIGHT PACKAGE:
;; ===========================================================
;; https://elpa.gnu.org/packages/delight.html

(use-package delight
	     :ensure t)

;;; use-package.el ends here
