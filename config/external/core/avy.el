;;; avy.el ---- Jump to things in Emacs tree-style

;;; Commentary:
;; ============================================================
;; AVY PACKAGE
;; ============================================================
;; `avy' is a GNU Emacs package for jumping to visible text using a
;; char-based decision tree.
;; https://github.com/abo-abo/avy

;;; Code:
(use-package avy
  :ensure t
  :bind
  ("C-;" . 'avy-goto-char))

;;; avy.el ends here
