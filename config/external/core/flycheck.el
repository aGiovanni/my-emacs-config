;;; flycheck.el ---- Syntax checking for GNU Emacs

;;; Commentary:
;; ============================================================
;; FLYCHECK PACKAGE
;; ============================================================
;; Flycheck is a modern on-the-fly syntax checking extension for GNU
;; Emacs, intended as replacement for the older Flymake extension
;; which is part of GNU Emacs.

;; http://www.flycheck.org/en/latest/

;;; Code:
(use-package flycheck
  :ensure t
  :delight
  :hook
  (prog-mode . flycheck-mode)
  :config
  (defun my/use-eslint-from-node-modules ()
    "[Javascript] Add local eslint from node_modules folder."
    (let* ((root (locate-dominating-file
                  (or (buffer-file-name) default-directory)
                  "node_modules"))
           (eslint (and root
                        (expand-file-name "node_modules/eslint/bin/eslint.js"
                                          root))))
      (when (and eslint (file-executable-p eslint))
        (setq-local flycheck-javascript-eslint-executable eslint))))
  
  (add-hook 'flycheck-mode-hook #'my/use-eslint-from-node-modules))

;;; flycheck.el ends here
