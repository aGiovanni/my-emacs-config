;;; interface.el ---- Configuración de la interfaz:

;;; Commentary:
;; Configuración de la interfaz de Emacs:

;;; Code:

;; Quitamos la barra de herramientas:
(tool-bar-mode -1)

;; Quitamos la barra de menús:
(menu-bar-mode -1)

;; Quitamos el scrollbar derecho:
(scroll-bar-mode -1) ;; No funciona al crear nuevos 'frames'
;; (add-hook 'after-make-frame-functions 'custom/disable-scrollbars)

;; Mostramos la línea y columna en donde se encuentra el cursor:
(column-number-mode t)

;; Destacamos los paréntesis en donde se encuentra el cursor:
(show-paren-mode)

;; Resaltamos la línea en donde se encuentra el cursor:
(global-hl-line-mode t)

;; Mostramos el No. de línea relativo (Solo emacs>26):
;; (add-hook 'prog-mode-hook
;; 	  (lambda ()
;; 	    (setq display-line-numbers 'relative)))

;; Generamos símbolos en palabras como lambda:
(global-prettify-symbols-mode t)

;; Algunas configuraciones para cambiar cómo Emacs maneja el scrolling:
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)) ;; Una línea a la vez
      mouse-wheel-progressive-speed nil
      mouse-wheel-follow-mouse t
      scroll-conservatively 10000)

;; Cambiamos la letra de la interfaz:
(set-frame-font "DejaVu Sans Mono 9" nil t)
(setq default-frame-alist '((font . "DejaVu Sans Mono-9")))

;;; interface.el ends here
