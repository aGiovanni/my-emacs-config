;;; functions.el ---- A set of functions to complement Emacs' functionality.

;;; Commentary:

;;; Code:
(defun my-org-screenshot ()
  "`my-org-screenshot' Take a screenshot into a time stamped
unique-named file in the same directory as the org-buffer and
insert a link to this file."
  (interactive)
  (suspend-frame)
  (setq filename
        (concat
         (make-temp-name
          (concat (file-name-nondirectory (buffer-file-name))
                  "_imgs/"
                  (format-time-string "%Y%m%d_%H%M%S_")) ) ".png"))
  (unless (file-exists-p (file-name-directory filename))
    (make-directory (file-name-directory filename)))
  ;; take screenshot
  (if (eq system-type 'darwin)
      (call-process "screencapture" nil nil nil "-i" filename))
  (if (eq system-type 'gnu/linux)
      (call-process "import" nil nil nil filename))
  ;; insert into file if correctly taken
  (if (file-exists-p filename)
    (insert (concat "[[file:" filename "]]"))))

;;; functions.el ends here
