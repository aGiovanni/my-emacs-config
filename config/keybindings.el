;;; keybindings.el ---- Combinaciones del teclado:

;;; Commentary:
;; Configuración personal de algunas combinaciones de teclado:

;;; Code:

;; Agregamos algunas combinaciones para la agenda de org-mode:
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-switchb)

;; Simplificamos la manera de cambiar entre ventanas:
(global-set-key (kbd "M-o") 'other-window)
;; (global-set-key (kbd "C-M-o") 'other-frame)

;; Simplificamos la manera de crear ventanas:
(global-set-key (kbd "M-2") 'split-window-below)
(global-set-key (kbd "M-3") 'split-window-right)

;; Eliminamos la ventana en donde se encuentra el cursor:
(global-set-key (kbd "M-0") 'delete-window)

;; Eliminamos las demás ventanas usando M-1:
(global-set-key (kbd "M-1") 'delete-other-windows)

;; Simplificamos el "scrolling":
;; (global-set-key (kbd "M-p") 'scroll-down-command)
;; (global-set-key (kbd "M-n") 'scroll-up-command)

;;; keybindings.el ends here
