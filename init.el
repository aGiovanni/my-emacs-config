;;; init.el ---- Configuración personal de Emacs:

;;; Commentary:
;; Aquí se cargan los archivos de configuración:

;;; Code:

;;; ========== Archivos a cargar: ==========

;;; Guardamos todas las configuraciones generadas por Emacs en 'custom.el'

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq custom-file
      (expand-file-name "~/.emacs.d/custom.el"))
(load custom-file)

;; Definimos la carpeta por defecto que utiliza Emacs:
(defvar user-home-dir (concat (getenv "HOME") "/"))
(setq user-emacs-directory (concat user-home-dir ".emacs.d/"))

;; Definimos en donde se cargan las configuraciones:
(defvar user-config-dir (concat user-emacs-directory "config/"))
(defvar user-external-dir (concat user-config-dir "external/"))

;;; Se cargan las demás configuraciones personales:
(load (concat user-config-dir "settings"))
(load (concat user-config-dir "interface"))
(load (concat user-config-dir "keybindings"))
(load (concat user-config-dir "functions"))

;; Ahora se cargan los archivos de configuración de paquetes externos (de 'use-package')
(load (concat user-external-dir "loader.el"))

;;; init.el ends here
